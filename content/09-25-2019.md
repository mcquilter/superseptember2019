---
title: "Empress"
cover: "empress.jpg"
date: "2019-09-25"
category: "travel"
tags:
    - victoria
    - BC
    - vancouver
---

## Empress Concierge 

breakfast
buffet  de concierge

dinner
buffet  de concierge

### Album Re-cap

[20190928](https://museumofpop-20190928.mcquilter.com)

[20190927](https://20190927.mcquilter.com)

[20190926](https://riseofthejaguar-20190926.mcquilter.com)

[20190925](https://empress-20190925.mcquilter.com)

[20190924](https://butchartgardensseptember2019.mcquilter.com)

[quince](https://quince.mcquilter.com)

[victoriabc-2019-09](https://victoriabc-2019-09.mcquilter.com)

[OlympicPeninsula2019](http://olympicpeninsula2019.mcquilter.com/)

[20190923](http://redlion-20190923.mcquilter.com/)

[20190921](http://goonies-20190921.mcquilter.com/)
